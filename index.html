<!doctype html>
<html>
	<head>
		<!-- Page setup -->
		<meta charset="utf-8">
		<title>Brian2Lava</title>
		<meta name="description" content="A Brian 2 interface for the neuromorphic computing framework Lava">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
		<link rel="icon" type="image/png" href="favicon.png">

		<!-- Librariy CSS -->
		<link href="lib/bootstrap/5.3.0/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="lib/highlight.js/11.7.0/default.min.css" rel="stylesheet" type="text/css" />

		<!-- Custom styles -->
		<link href="css/fonts.css" rel="stylesheet" type="text/css" />
		<link href="css/style.css" rel="stylesheet" type="text/css" />

		<!-- Librariy JS -->
		<script src="lib/jquery/3.4.1/jquery.min.js"></script>
		<script src="lib/bootstrap/5.3.0/bootstrap.bundle.min.js"></script>
		<script src="lib/highlight.js/11.7.0/highlight.min.js"></script>

	</head>
  
	<body>

		<header>
			<div class="container">
				<h1>Brian2Lava</h1>
				<p>An open-source Brian 2 interface for the neuromorphic computing framework Lava</p>
				
				<nav>
					<ul class="main-menu">
						<li><a href="#home" title="Home page">Home</a></li>
						<li><a target="_blank" href="https://brian2lava.gitlab.io/docs/introduction/examples.html" title="Getting started with an example">&nearr;Example code</a></li>
						<li><a target="_blank" href="https://brian2lava.gitlab.io/docs/" title="Brian2Lava Documentation">&nearr;Documentation</a></li>
						<li><a href="#team" title="Brian2Lava core team">Team</a></li>
						<li><a href="#resources" title="Resources for Brian2Lava">Resources</a></li>
					</ul>
				</nav>
			<div>
		</header>
	
		<main class="container">
			<div class="content">
				
				<div id="home" class="content-region hide">

					<h2>What is Brian2Lava?</h2>
					<div class="row">
						<div class="col">
							<p><a target="_blank" title="Brian Website" href="https://briansimulator.org/">Brian</a> is an open-source Python package developed and used by the computational neuroscience community to simulate spiking neural networks. The goal of the <strong>Brian2Lava</strong> open-source project is to develop a Brian device interface for the neuromorphic computing framework <a target="_blank" title="Lava Website" href="https://lava-nc.org/">Lava</a>, in order to facilitate deployment of brain-inspired algorithms on Lava-supported hardware and emulator backends. The main purpose of Brian2Lava is to accelerate neuromorphic model prototyping by</p>
							<ul>
								<li>harnessing the power of <strong>differential-equation-based model definitions</strong>, </li>
								<li>enabling the <strong>execution of existing Brian models</strong>, and </li>
								<li>facilitating <strong>deployment on different backend systems</strong> via Lava.</li>
							</ul>
							<p>The features of Brian2Lava can, moreover, be beneficial for any stage of model simulation beyond prototyping. Thus, the project aims to <strong>reduce entry barriers</strong> and <strong>enable more efficient algorithm development</strong> for using neuromorphic hardware such as Loihi 2.</p>
						</div>
						<div class="col">
							<img style="padding: 30px 20px; border: 1px solid #ccc;" class="img-fluid" src="img/overview.png" alt="Brian2Lava structure" />
						</div>
					</div>

					<h2>Getting started</h2>
					<p>Below you'll find information on how to quickly get Brian2Lava to run. For further information, please visit the <a target="_blank" href="https://brian2lava.gitlab.io/docs/" title="Brian2Lava Documentation">documentation</a></li>.</p>

					<div class="alert alert-warning" role="alert">
						Brian2Lava is still in its testing phase. Please feel free to <a target="_blank" href="https://gitlab.com/brian2lava/brian2lava/-/issues" title="Brian2Lava Repository — Issues">report issues or feature requests</a>.
					</div>
					<div class="alert alert-warning" role="alert">
						At the moment, due to legal restrictions, models for the <code>Loihi2</code> backend may just be provided to members of the <a target="_blank" href="https://intel-ncl.atlassian.net/wiki/spaces/INRC/pages/1784807425/Join+the+INRC" title="INRC">Intel Neuromorphic Research Community</a>. Thus, the public version of Brian2Lava currently contains models for the <code>CPU</code> backend only.
					</div>
					<h4>Installation</h4>

					<p>Brian2Lava can be easily installed via the Python Package Index (<code>pip</code>):</p>
					<pre><code class="language-bash">python3 -m pip install brian2lava</code></pre>
					<p>Note: <code>conda</code> support may be added at a later point.</p>
					<p>For the latest source code, please visit <a target="_blank" title="Brian2Lava GitLab" href="https://gitlab.com/brian2lava/brian2lava">gitlab.com/brian2lava/brian2lava</a>. If you run from source code, make sure that you have added your path to the source code to <code>PYTHONPATH</code>, for example, via:</p>
					<pre><code class="language-bash">export PYTHONPATH=~/brian2lava</code></pre>

					<h4>Prerequisites</h4>

					<p>Make sure that you have installed <a target="_blank" title="Brian 2 on GitHub" href="https://github.com/brian-team/brian2">Brian 2</a> (recommended >= 2.7.1) and <a target="_blank" title="Lava on GitHub" href="https://github.com/lava-nc/lava">Lava</a> (recommended >= 0.10.0, or lava_loihi-0.7.0).</p>

					<h4>Import package and set device</h4>
					<p>Using Brian2Lava within Brian 2 only requires two steps.</p>
					<p>First, import the package:</p>
					<pre><code class="language-python">import brian2lava</code></pre>
					<p>Second, set the <code>lava</code> device with the desired hardware backend and mode:</p>
					<pre><code class="language-python">set_device('lava', hardware='CPU', mode='preset')</code></pre>
					<p>In principle, this can already run your Brian simulation on the Lava engine. However, you may have to use a few additional settings to further specify how the code for the simulation is generated and executed. Please see the <a target="_blank" href="https://brian2lava.gitlab.io/docs/user_guide/import_set_device.html" title="Brian2Lava Documentation — Importing and device setting">documentation</a> for more information.</p>
					<p>You may want to continue by considering the example code provided <a target="_blank" title="Example code" href="https://brian2lava.gitlab.io/docs/introduction/examples.html">here</a>.</p>

					<h2>How to contribute</h2>
					<p></p>
					<div class="row">
						<div class="col-12 col-sm-6 col-lg-4">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title">Discussion, ideas, feedback</h5>
									<p class="card-text">If you have <strong>ideas</strong>, for example for new features, or want to give us <strong>feedback</strong> on the existing implementation, please visit the <a target="_blank" title="Brian2Lava Repository — Issues" href="https://gitlab.com/brian2lava/brian2lava/-/issues">issues section in our repository</a>.</p>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-lg-4">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title">Share your models</h5>
									<p class="card-text">If you actively use Brian2Lava, we'll be happy if you <strong>share your models</strong>! This will serve to accelerate the development of neuromorphic algorithms. Please <a href="mailto:jannik.luboeinski@med.uni-goettingen.de" title="E-mail to corresponding developer">contact us</a>.</p>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-lg-4">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title">Report bugs</h5>
									<p class="card-text">If you find a <strong>bug</strong>, we'll be happy if you <strong>report</strong> it as an <a target="_blank" title="Brian2Lava Repository — Issues" href="https://gitlab.com/brian2lava/brian2lava/-/issues">issue</a> in the Brian2Lava repository.</p>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-lg-4">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title">Merge/pull request</h5>
									<p class="card-text">If you'd like to make a concrete <strong>code contribution</strong>, please feel free to <a target="_blank" title="Brian2Lava Repository — Merge Requests" href="https://gitlab.com/brian2lava/brian2lava/-/merge_requests">open a merge/pull request</a>. You can find more information on contributing <a target="_blank" title="Brian2Lava Documentation — Contributing" href="https://brian2lava.gitlab.io/docs/dev_guide/contributing.html">here</a>.</p>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-lg-4">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title">Develop with us</h5>
									<p class="card-text">If you're interested in <strong>development</strong>, please <a href="mailto:jannik.luboeinski@med.uni-goettingen.de" title="E-mail to corresponding developer">contact us</a>. We can meet and discuss how to <strong>work together</strong>.</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div id="resources" class="content-region hide">
					<h2>Resources</h2>
					<p>Here we collect some resources, like publications and links corresponding Brian 2, Lava and Brian2Lava.</p>

					<div class="row">

						<div class="col-12 col-sm-6 col-lg-4">
							<h3>Brian2Lava</h3>
							<ul>
								<li><a title="Brian2Lava Website" href="https://brian2lava.gitlab.io/">Website</a></li>
								<li><a target="_blank" title="Brian2Lava Repository" href="https://gitlab.com/brian2lava/brian2lava">Repository</a></li>
								<li><a target="_blank" title="Brian2Lava Wiki" href="https://gitlab.com/brian2lava/brian2lava/-/wikis/home">Wiki</a></li>
								<li><a target="_blank" title="Brian2Lava Documentation" href="https://brian2lava.gitlab.io/docs/">Documentation</a></li>
								<!--li><a target="_blank" title="Brian2Lava Paper">Paper (coming soon)</a></li-->
								<li><a target="_blank" title="Slides INRC Forum Talk by Jannik Luboeinski on 2023-11-28" href="https://brian2lava.gitlab.io/files/Brian2Lava_2023-11-28.pdf">Slides INRC Forum Talk 2023-11-28</a></li>
								<li><a target="_blank" title="Tetzlab Group" href="https://tetzlab.com/">Tetzlab Group Website</a></li>
							</ul>
						</div>

						<div class="col-12 col-sm-6 col-lg-4">
							<h3>Brian 2</h3>
							<ul>
								<li><a target="_blank" title="Brian Website" href="https://briansimulator.org/">Website</a></li>
								<li><a target="_blank" title="Brian Repository" href="https://github.com/brian-team/brian2/">Repository</a></li>
								<li><a target="_blank" title="Brian Forum" href="https://brian.discourse.group/">Forum</a></li>
								<li><a target="_blank" title="Brian Documentation" href="https://brian2.readthedocs.io/">Documentation</a></li>
								<li><a target="_blank" title="Brian Paper" href="https://doi.org/10.7554/eLife.47314">Paper</a></li>
							</ul>
						</div>

						<div class="col-12 col-sm-6 col-lg-4">
							<h3>Lava & Loihi</h3>
							<ul>
								<li><a target="_blank" title="Lava Website" href="https://lava-nc.org/">Website</a></li>
								<li><a target="_blank" title="Lava Repository" href="https://github.com/lava-nc/lava">Repository</a></li>
								<li><a target="_blank" title="Intel neuromorhphic computing website" href="https://www.intel.com/content/www/us/en/research/neuromorphic-computing.html">Intel project website</a></li>
								<li><a target="_blank" title="Loihi 1 Paper" href="https://doi.org/10.1109/MM.2018.112130359">Loihi 1 Paper</a></li>
								<li><a target="_blank" title="Loihi 1 Programming Paper" href="https://doi.org/10.1109/MC.2018.157113521">Loihi 1 Programming Paper</a></li>
								<li><a target="_blank" title="Loihi Review" href="https://doi.org/10.1109/JPROC.2021.3067593">Loihi Review</a></li>
								<li><a target="_blank" title="Loihi 2 Technology Brief" href="https://www.intel.com/content/www/us/en/research/neuromorphic-computing-loihi-2-technology-brief.html">Loihi 2 Technology Brief</a></li>
							</ul>
						</div>

					</div>
				</div>

				<div id="team" class="content-region hide">
					<h2>Contributors to code and/or concept</h2>
					<div class="row">

						<div class="col-12 col-sm-6 col-lg-4">
							<div class="card">
								<div class="card-body">
									<div class="card-flex">
										<div class="card-left">
											<img src="img/team/jannik.jpg" />
										</div>
										<div class="card-right">
											<h5 class="card-title">Dr. Jannik Luboeinski</h5>
											<p class="card-text">Lead developer (since 07/2023). Computational neuroscience expert.</p>
										</div>
									</div>
									<a target="_blank" title="LinkedIn" href="https://www.linkedin.com/in/jannik-luboeinski-630a35243/" class="card-link">LinkedIn</a>,
									<a target="_blank" title="ResearchGate" href="https://www.researchgate.net/profile/Jannik-Luboeinski" class="card-link">RG</a>,
									<a target="_blank" title="GitHub" href="https://github.com/jlubo" class="card-link">GitHub</a>,
									<a target="_blank" title="GitLab" href="https://gitlab.com/jlubo" class="card-link">GitLab</a>
								</div>
							</div>
						</div>

						<div class="col-12 col-sm-6 col-lg-4">
							<div class="card">
								<div class="card-body">
									<div class="card-flex">
										<div class="card-left">
											<img src="img/team/francesco.jpg" />
										</div>
										<div class="card-right">
											<h5 class="card-title">Francesco Negri</h5>
											<p class="card-text">Software developer.</p>
										</div>
									</div>
									<a target="_blank" title="LinkedIn" href="https://www.linkedin.com/in/francesco-negri-43bb93183/" class="card-link">LinkedIn</a>,
									<a target="_blank" title="GitHub" href="https://github.com/FrancescoNegri" class="card-link">GitHub</a>,
									<a target="_blank" title="GitLab" href="https://gitlab.com/ngr.francesco" class="card-link">GitLab</a>
								</div>
							</div>
						</div>

					</div>

					<div class="row">
						<div class="col-12 col-sm-6 col-lg-4">
							<div class="card">
								<div class="card-body">
									<div class="card-flex">
										<div class="card-left">
											<img src="img/team/carlo.jpg" />
										</div>
										<div class="card-right">
											<h5 class="card-title">Dr. Carlo Michaelis</h5>
											<p class="card-text">Lead developer until 07/2023. Loihi expert.</p>
										</div>
									</div>
									<a target="_blank" title="LinkedIn" href="https://www.linkedin.com/in/carlo-michaelis/" class="card-link">LinkedIn</a>,
									<a target="_blank" title="Google Scholar" href="https://scholar.google.de/citations?user=Fg9UpPsAAAAJ" class="card-link">Google Scholar</a>,
									<a target="_blank" title="GitHub" href="https://github.com/sagacitysite" class="card-link">GitHub</a>,
									<a target="_blank" title="GitLab" href="https://gitlab.com/carlomichaelis" class="card-link">GitLab</a>
								</div>
							</div>
						</div>

						<div class="col-12 col-sm-6 col-lg-4">
							<div class="card">
								<div class="card-body">
									<div class="card-flex">
										<div class="card-left">
											<img src="img/team/winni.jpg" />
										</div>
										<div class="card-right">
											<h5 class="card-title">Winfried Oed</h5>
											<p class="card-text">Software Developer.</p>
										</div>
									</div>
									<a target="_blank" title="GitHub" href="https://github.com/Winnus" class="card-link">GitHub</a>,
									<a target="_blank" title="GitLab" href="https://gitlab.com/Winnus" class="card-link">GitLab</a>
								</div>
							</div>
						</div>
					</div>

					<div class="row">

						<div class="col-12 col-sm-6 col-lg-4">
							<div class="card">
								<div class="card-body">
									<div class="card-flex">
										<div class="card-left">
											<img src="img/team/andrew.jpg" />
										</div>
										<div class="card-right">
											<h5 class="card-title">Andrew Lehr</h5>
											<p class="card-text">Computational neuroscience expert.</p>
										</div>
									</div>
									<a target="_blank" title="Google Scholar" href="https://scholar.google.de/citations?user=zpu9tugAAAAJ" class="card-link">Google Scholar</a>,
									<a target="_blank" title="GitHub" href="https://github.com/andrewlehr" class="card-link">GitHub</a>,
									<a target="_blank" title="GitLab" href="https://gitlab.com/andrewlehr" class="card-link">GitLab</a>
								</div>
							</div>
						</div>

						<div class="col-12 col-sm-6 col-lg-4">
							<div class="card">
								<div class="card-body">
									<div class="card-flex">
										<div class="card-left">
											<img src="img/team/tristan.jpg" />
										</div>
										<div class="card-right">
											<h5 class="card-title">Dr. Tristan Stöber</h5>
											<p class="card-text">Computational neuroscience expert.</p>
										</div>
									</div>
									<a target="_blank" title="LinkedIn" href="https://www.linkedin.com/in/tristan-manfred-st%C3%B6ber-ph-d-941b34160/" class="card-link">LinkedIn</a>,
									<a target="_blank" title="ResearchGate" href="https://www.researchgate.net/profile/Tristan-Stoeber-2" class="card-link">RG</a>,
									<a target="_blank" title="Google Scholar" href="https://scholar.google.de/citations?user=JghW0NoAAAAJ" class="card-link">Google Scholar</a>,
									<a target="_blank" title="GitHub" href="https://github.com/tristanstoeber" class="card-link">GitHub</a>,
									<a target="_blank" title="GitLab" href="https://gitlab.com/tristan.stoeber" class="card-link">GitLab</a>
								</div>
							</div>
						</div>

					</div>

					<div class="row">
						<div class="col-12 col-sm-6 col-lg-4">
							<div class="card">
								<div class="card-body">
									<div class="card-flex">
										<div class="card-left">
											<img src="img/team/christian.jpg" />
										</div>
										<div class="card-right">
											<h5 class="card-title">Prof. Dr. Christian Tetzlaff</h5>
											<p class="card-text">Computational neuroscience expert.</p>
										</div>
									</div>
									<a target="_blank" title="ResearchGate" href="https://www.researchgate.net/profile/Christian-Tetzlaff" class="card-link">RG</a>,
									<a target="_blank" title="Google Scholar" href="https://scholar.google.de/citations?user=kUE_vAsAAAAJ" class="card-link">Google Scholar</a>
								</div>
							</div>
						</div>
					</div>

				</div>

				<div id="impressum" class="content-region hide">
					<h2>Legal notice</h2>
					<div class="row">

						<p>
							Corresponding developer:<br/>
							<span style="display:inline-block; width: 30px;"></span>Dr. Jannik Luboeinski
						<p></p>
						<p>
							E-Mail:<br/>
							<span style="display:inline-block; width: 30px;"></span>jannik.luboeinski@med.uni-goettingen.de
						</p>
						<p></p>
						<p>
							Address:<br/>
							<span style="display:inline-block; width: 30px;"></span>Department of Neuro- and Sensory Physiology<br/>
							<span style="display:inline-block; width: 30px;"></span>University of Göttingen Medical Center<br/>
							<span style="display:inline-block; width: 30px;"></span>Humboldtallee 23<br/>
							<span style="display:inline-block; width: 30px;"></span>37073 Göttingen<br/>
							<span style="display:inline-block; width: 30px;"></span>Germany
						</p>
					</div>
				</div>
			</div>
		</main>

		<footer>
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-6 col-lg-4">
						<h3>Links</h3>
						<ul>
							<li><a target="_blank" title="Brian2Lava GitLab Repository" href="https://gitlab.com/brian2lava/brian2lava">&nearr; GitLab Brian2Lava</a></li>
							<li><a target="_blank" title="Brian2Lava Documentation" href="https://brian2lava.gitlab.io/docs/">&nearr; Docs Brian2Lava</a></li>
							<li><a target="_blank" title="Brian 2 Website" href="https://briansimulator.org/">&nearr; Website Brian 2</a></li>
							<li><a target="_blank" title="Lava Website" href="https://lava-nc.org/">&nearr; Website Lava</a></li>
						</ul>
					</div>
					<div class="col-12 col-sm-6 col-lg-4">
						<h3>Contact</h3>
						<ul>
							<li><a href="mailto:jannik.luboeinski@med.uni-goettingen.de" title="E-mail to corresponding developer">E-mail</a></li>
							<li><a href="#impressum" title="Legal notice or Impressum">Legal notice</a></li>
						</ul>
					</div>
				</div>
			</div>
		</footer>
    
		<!-- Custom JS -->
		<script type="text/javascript" src="script.js"></script>
		<script>
			hljs.highlightAll();
		</script>
    
	</body>
</html>
